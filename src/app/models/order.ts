import { OrderStatus } from './order-status';
import { Customer } from './customer';
import { Warehouse } from './warehouse';
import { OrderDetail } from './order-detail';

export class Order {
    id: number;
    status: OrderStatus;
    customer: Customer;
    warehouse: Warehouse;
    orderDetail: OrderDetail[];
    shippingDate: string;
    deliveryDate: string;
    createdAt: string;
    deleted: boolean;
}
