import { Product } from './product';
import { Warehouse } from './warehouse';

export class ProductWarehouse {
    id: number;
    product: Product;
    warehouse: Warehouse;
    quantity: number;
    createdAt: string;
    deleted: boolean;
}
