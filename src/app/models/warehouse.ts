import { Product } from './product';
import { ProductWarehouse } from './product-warehouse';

export class Warehouse {
    id: number;
    name: string;
    warehouseIdentifier: string;
    address: string;
    products: ProductWarehouse[];
    createdAt: string;
    deleted: boolean;
}
