import { Order } from './order';
import { Product } from './product';

export class OrderDetail {
    id: number;
    order: Order;
    product: Product;
    quantity: number;
    createdAt: string;
    deleted: boolean;
}
