export class Product  {
    id: number;
    sku: string;
    name: string;
    description: string;
    cretedAt: string;
    enabled: boolean;
    deleted: boolean;
}
