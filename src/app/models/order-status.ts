export class OrderStatus {
    id: number;
    name: string;
    description: string;
    createdAt: string;
    deleted: boolean;
}
