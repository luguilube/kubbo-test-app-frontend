export class Customer {
    id: number;
    name: string;
    surname: string;
    dni: string;
    phone: string;
    email: string;
    birthdate: string;
    address: string;
    createdAt: string;
    deleted: boolean;
}
