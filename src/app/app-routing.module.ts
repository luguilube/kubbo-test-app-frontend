import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './components/products/products.component';
import { WarehousesComponent } from './components/warehouses/warehouses.component';
import { WarehousesShowComponent } from './components/warehouses/warehouses-show/warehouses-show.component';
import { OrdersComponent } from './components/orders/orders.component';
import { CustomersComponent } from './components/customers/customers.component';
import { OrdersFormComponent } from './components/orders/orders-form/orders-form.component';


const routes: Routes = [
  {path: "", pathMatch: 'full', redirectTo: 'products'},
  {path: 'products', component: ProductsComponent},
  {path: 'warehouses', component: WarehousesComponent},
  {path: "warehouses/show/:id", component: WarehousesShowComponent},
  {path: 'customers', component: CustomersComponent},
  {path: 'orders', component: OrdersComponent},
  {path: 'orders/form', component: OrdersFormComponent},
  {path: 'orders/form/:id', component: OrdersFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
