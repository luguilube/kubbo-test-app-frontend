import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 

// Angular Material Imports
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatMomentDateModule } from "@angular/material-moment-adapter";

// Kubbo App Components
import { LayoutModule } from './layout/layout.module';
import { ProductsComponent } from './components/products/products.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { ModalComponent } from './components/products/modal/modal.component';
import { WarehousesComponent } from './components/warehouses/warehouses.component';
import { WarehousesModalComponent } from './components/warehouses/warehouses-modal/warehouses-modal.component';
import { WarehousesShowComponent } from './components/warehouses/warehouses-show/warehouses-show.component';
import { WarehousesStockModalComponent } from './components/warehouses/warehouses-show/warehouses-stock-modal/warehouses-stock-modal.component';
import { OrdersComponent } from './components/orders/orders.component';
import { CustomersComponent } from './components/customers/customers.component';
import { CustomerModalComponent } from './components/customers/customer-modal/customer-modal.component';
import { OrdersFormComponent } from './components/orders/orders-form/orders-form.component';
import { OrdersModalComponent } from './components/orders/orders-modal/orders-modal.component';
import { StatusUpdateModalComponent } from './components/orders/status-update-modal/status-update-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ModalComponent,
    WarehousesComponent,
    WarehousesModalComponent,
    WarehousesShowComponent,
    WarehousesStockModalComponent,
    OrdersComponent,
    CustomersComponent,
    CustomerModalComponent,
    OrdersFormComponent,
    OrdersModalComponent,
    StatusUpdateModalComponent
  ],
  entryComponents: [ 
    ModalComponent,
    WarehousesModalComponent,
    CustomerModalComponent,
    OrdersModalComponent,
    StatusUpdateModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatCardModule,
    MatPaginatorModule,
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatTabsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    MatSelectModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
