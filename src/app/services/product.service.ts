import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { APP_URL_BASE } from '../config/app-constants';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private baseEndpoint = APP_URL_BASE + '/products';
  protected headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(private http: HttpClient) { }

  public findAll(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.baseEndpoint}/list`, { headers: this.headers });
  }

  public findAllPaginated(page: string, size: string): Observable<any> {
    const params = new HttpParams() 
    .set('page', page)
    .set('size', size);

    return this.http.get<any>(`${this.baseEndpoint}/paginated-list`, {params: params});
  }

  public store(product: Product): Observable<Product> {
    return this.http.post<Product>(`${this.baseEndpoint}/store`, product, { headers: this.headers });
  }

  public update(product: Product): Observable<Product> {
    return this.http.put<Product>(`${this.baseEndpoint}/update/${product.id}`, product, { headers: this.headers });
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseEndpoint}/delete/${id}`);
  }
}
