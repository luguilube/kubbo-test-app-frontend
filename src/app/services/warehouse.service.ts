import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Warehouse } from '../models/warehouse';
import { Observable } from 'rxjs';
import { ProductWarehouse } from '../models/product-warehouse';
import { Product } from '../models/product';
import { APP_URL_BASE } from '../config/app-constants';

@Injectable({
  providedIn: 'root'
})
export class WarehouseService {

  private baseEndpoint = APP_URL_BASE + '/warehouses';
  protected headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(private http: HttpClient) { }

  public findAll(): Observable<Warehouse[]> {
    return this.http.get<Warehouse[]>(`${this.baseEndpoint}/list`, { headers: this.headers });
  }

  public findAllPaginated(page: string, size: string): Observable<any> {
    const params = new HttpParams() 
    .set('page', page)
    .set('size', size);

    return this.http.get<any>(`${this.baseEndpoint}/paginated-list`, {params: params});
  }

  public store(warehouse: Warehouse): Observable<Warehouse> {
    return this.http.post<Warehouse>(`${this.baseEndpoint}/store`, warehouse, { headers: this.headers });
  }

  public get(id: number): Observable<Warehouse> {
    return this.http.get<Warehouse>(`${this.baseEndpoint}/get/${id}`);
  }

  public update(warehouse: Warehouse): Observable<Warehouse> {
    return this.http.put<Warehouse>(`${this.baseEndpoint}/update/${warehouse.id}`, warehouse, { headers: this.headers });
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseEndpoint}/delete/${id}`);
  }

  public stockManagement(warehouse: Warehouse): Observable<Warehouse> {
    return this.http.put<Warehouse>(`${this.baseEndpoint}/stock-management/${warehouse.id}`, warehouse, { headers: this.headers });
  }

  public productDeleteFromStock(warehouseId: number, procutoWId: number): Observable<ProductWarehouse[]> {
    return this.http.delete<ProductWarehouse[]>(`${this.baseEndpoint}/product-delete-from-stock/${warehouseId}/${procutoWId}`);
  }

  public getAvailableProductsNotAssigned(id: number): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.baseEndpoint}/available-products-not-assigned/${id}`);
  }

  public addProductToWarehouse(productWarehouse: ProductWarehouse): Observable<Warehouse> {
    return this.http.put<Warehouse>(`${this.baseEndpoint}/add-product-to-warehouse`, productWarehouse, { headers: this.headers});
  }
}
