import { Injectable } from '@angular/core';
import { APP_URL_BASE } from '../config/app-constants';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Order } from '../models/order';
import { OrderStatus } from '../models/order-status';
import { Warehouse } from '../models/warehouse';
import { Customer } from '../models/customer';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private baseEndpoint = APP_URL_BASE + '/orders';
  protected headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(private http: HttpClient) { }

  public findAll(): Observable<Order[]> {
    return this.http.get<Order[]>(`${this.baseEndpoint}/list`, { headers: this.headers });
  }

  public findAllPaginated(page: string, size: string): Observable<any> {
    const params = new HttpParams() 
    .set('page', page)
    .set('size', size);

    return this.http.get<any>(`${this.baseEndpoint}/paginated-list`, {params: params});
  }

  public store(order: Order): Observable<Order> {
    return this.http.post<Order>(`${this.baseEndpoint}/store`, order, { headers: this.headers });
  }

  public findById(id: number): Observable<Order> {
    return this.http.get<Order>(`${this.baseEndpoint}/get/${id}`);
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseEndpoint}/delete/${id}`);
  }

  public getStatusList(): Observable<OrderStatus[]> {
    return this.http.get<OrderStatus[]>(`${this.baseEndpoint}/get-status-list`);
  }

  public getWarehousesList(): Observable<Warehouse[]> {
    return this.http.get<Warehouse[]>(`${this.baseEndpoint}/get-warehouses-list`);
  }

  public getProductsByWarehouse(warehouseId: number): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.baseEndpoint}/get-products-by-warehouse/${warehouseId}`);
  }

  public getCustomersList(): Observable<Customer[]> {
    return this.http.get<Customer[]>(`${this.baseEndpoint}/get-customers-list`);
  }

  public getStockByWarehouseAndProduct(warehouseId: number, productId: number): Observable<number> {
    return this.http.get<number>(`${this.baseEndpoint}/get-stock-from-warehouse-for-product/${warehouseId}/${productId}`);
  }

  public orderStatusUpdate(orderId: number, statusId: number): Observable<Order> {
    return this.http.put<Order>(`${this.baseEndpoint}/status-update/${orderId}/${statusId}`, { headers: this.headers });
  }
}
