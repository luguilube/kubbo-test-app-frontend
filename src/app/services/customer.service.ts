import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from '../models/customer';
import { APP_URL_BASE } from '../config/app-constants';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private baseEndpoint = APP_URL_BASE + '/customers';
  protected headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(private http: HttpClient) { }

  public findAll(): Observable<Customer[]> {
    return this.http.get<Customer[]>(`${this.baseEndpoint}/list`, { headers: this.headers });
  }

  public findAllPaginated(page: string, size: string): Observable<any> {
    const params = new HttpParams() 
    .set('page', page)
    .set('size', size);

    return this.http.get<any>(`${this.baseEndpoint}/paginated-list`, {params: params});
  }

  public store(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(`${this.baseEndpoint}/store`, customer, { headers: this.headers });
  }

  public update(customer: Customer): Observable<Customer> {
    return this.http.put<Customer>(`${this.baseEndpoint}/update/${customer.id}`, customer, { headers: this.headers });
  }

  public delete(id: number): Observable<void> {
    return this.http.delete<void>(`${this.baseEndpoint}/delete/${id}`);
  }
}
