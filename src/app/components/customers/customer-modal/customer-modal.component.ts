import { Component, OnInit, Inject } from '@angular/core';
import { Customer } from 'src/app/models/customer';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import * as moment from 'moment';

@Component({
  selector: 'app-customer-modal',
  templateUrl: './customer-modal.component.html',
  styleUrls: []
})
export class CustomerModalComponent implements OnInit {

  customer: Customer = new Customer();
  title: String = '';
  isShow: boolean = false;
  private emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  maxDate = new Date();
  public customerForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public modalRef: MatDialogRef<CustomerModalComponent>
  ) {
    
    this.customerForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      surname: new FormControl('', [Validators.required, Validators.minLength(3)]),
      dni: new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(12)]),
      phone: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(12)]),
      email: new FormControl('', [Validators.required, Validators.minLength(5), Validators.pattern(this.emailPattern)]),
      birthdate: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(250)]),
    });
  }

  get name() { return this.customerForm.get('name'); }
  get surname() { return this.customerForm.get('surname'); }
  get dni() { return this.customerForm.get('dni'); }
  get phone() { return this.customerForm.get('phone'); }
  get email() { return this.customerForm.get('email'); }
  get birthdate() { return this.customerForm.get('birthdate'); }
  get address() { return this.customerForm.get('address'); }

  ngOnInit(): void {
    this.customer = this.data.customer as Customer;
    this.title = this.data.title as string;
    this.isShow = this.data.isShow ? this.data.isShow as boolean : false;

    this.customerForm.get('name').setValue(this.customer.name);
    this.customerForm.get('surname').setValue(this.customer.surname);
    this.customerForm.get('dni').setValue(this.customer.dni);
    this.customerForm.get('phone').setValue(this.customer.phone);
    this.customerForm.get('email').setValue(this.customer.email);
    this.customerForm.get('birthdate').setValue(this.customer.birthdate);
    this.customerForm.get('address').setValue(this.customer.address);
  }

  cancel(): void {
    this.modalRef.close();
  }

  confirm(): void { 
    this.customer.name = this.customerForm.value.name;
    this.customer.surname = this.customerForm.value.surname;
    this.customer.dni = this.customerForm.value.dni;
    this.customer.phone = this.customerForm.value.phone;
    this.customer.email = this.customerForm.value.email;

    let myMoment: moment.Moment = moment(this.customerForm.value.birthdate);
    this.customer.birthdate = myMoment.format("yyyy-MM-DD");

    this.customer.address = this.customerForm.value.address;
    this.customerForm.reset();
  }
}
