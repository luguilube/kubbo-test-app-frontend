import { Component, OnInit, ViewChild } from '@angular/core';
import { Customer } from 'src/app/models/customer';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { CustomerService } from 'src/app/services/customer.service';
import { MatDialog } from '@angular/material/dialog';
import { CustomerModalComponent } from './customer-modal/customer-modal.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: []
})
export class CustomersComponent implements OnInit {

  customers: Customer[] = [];
  customer: Customer = new Customer();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  showColumns: string[] = ['position', 'dni', 'name', 'email', 'phone', 'actions'];
  pageSizeOptions: number[] = [3, 5, 10, 20, 30, 50];
  totalFields: number = 0;
  totalByPage: number = 3;
  currentPage: number = 0;

  constructor(
    private service: CustomerService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.rangesCalculate();
  }

  paginate(event: PageEvent): void {
    this.currentPage = event.pageIndex;
    this.totalByPage = event.pageSize;
    this.rangesCalculate();
  }

  private rangesCalculate() {
    this.service.findAllPaginated(this.currentPage.toString(), this.totalByPage.toString())
    .subscribe(page => {
      this.customers = page.content as Customer[];
      this.totalFields = page.totalElements as number;
      this.paginator._intl.itemsPerPageLabel = 'Registros por página';
    });
  }

  create(): void {
    this.customer = new Customer();
    const modalRef = this.dialog.open(CustomerModalComponent, {
      width: '750px',
      data: {customer: this.customer, title: 'Crear '} 
    });
    
    modalRef.afterClosed().subscribe(customer => {
      if (customer) {
        this.service.store(customer).subscribe(p => {
          this.customers = this.customers.concat(p);
          Swal.fire('Creado:', `Se ha registrado: ${customer.sku} - ${customer.name} con exito`, 'success');
        }); 
      }
    });
  }

  show(customer: Customer): void {
    const modalRef = this.dialog.open(CustomerModalComponent, {
      width: '750px',
      data: {customer: customer, title: 'Ver ', isShow: true} 
    });
  }
  
  edit(customer: Customer): void {
    const modalRef = this.dialog.open(CustomerModalComponent, {
      width: '750px',
      data: {customer: customer, title: 'Editar '} 
    });

    modalRef.afterClosed().subscribe(customer => {
      if (customer) {
        this.service.update(customer).subscribe(p => {
          this.showUpdatedItem(p as Customer);
          Swal.fire('Modificado:', `Se ha actualizado el customero sku: ${customer.dni} - ${customer.name} ${customer.surname} con exito`, 'success');
        }); 
      }
    });
  }

  delete(customer: Customer): void { 
    Swal.fire({
      title: 'Advertencia:',
      text: `¿Seguro que desea eliminar a ${customer.dni} - ${customer.name} ${customer.surname}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar'
    }).then((result) => {
      if (result.value) {
        this.service.delete(customer.id).subscribe(() => {
          this.customers = this.customers.filter(p => p.id !== customer.id);
          Swal.fire('Eliminado: ',`${customer.dni} - ${customer.name} ${customer.surname} eliminado con éxito`, "success");
        });
      }
    });
  }

  showUpdatedItem(newItem: Customer){
    let updateItem = this.customers.find(this.findIndexToUpdate, newItem.id);

    let index = this.customers.indexOf(updateItem);


    this.customers[index] = newItem;

  }

  findIndexToUpdate(newItem) { 
        return newItem.id === this;
  }

}
