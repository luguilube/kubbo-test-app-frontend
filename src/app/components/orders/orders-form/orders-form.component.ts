import { Component, OnInit, ViewChild } from '@angular/core';
import { Order } from 'src/app/models/order';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OrderService } from 'src/app/services/order.service';
import { Warehouse } from 'src/app/models/warehouse';
import { Customer } from 'src/app/models/customer';
import { Product } from 'src/app/models/product';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { OrderDetail } from 'src/app/models/order-detail';
import { MatDialog } from '@angular/material/dialog';
import { OrdersModalComponent } from '../orders-modal/orders-modal.component';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-orders-form',
  templateUrl: './orders-form.component.html',
  styleUrls: []
})
export class OrdersFormComponent implements OnInit {

  order: Order = new Order();
  orderDetail: OrderDetail[] = [];
  detail: OrderDetail = new OrderDetail;

  isShow: boolean = false;
  warehouses: Warehouse[] = [];
  customers: Customer[] = [];
  products: Product[] = [];
  orderForm: FormGroup;

  dataSource: MatTableDataSource<OrderDetail>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  showColumns: string[] =  [];
  pageSizeOptions: number[] = [3, 5, 10, 20, 30, 50];
  tabIndex = 0;

  dataSourceProduct: MatTableDataSource<Product>;
  @ViewChild(MatPaginator, { static: true }) paginatorProducts: MatPaginator;
  showProductColumns: string[] = ['position', 'sku', 'name', 'actions'];
  pageSizeProductsOptions: number[] = [3, 5, 10, 20, 30, 50];

  newQuantity: number = 0;

  constructor(
    private service: OrderService,
    public dialog: MatDialog,
    protected router: Router,
    private route: ActivatedRoute,
  ) {
    
  }

  get customer() { return this.orderForm.get('customer'); }
  get customerAddress() { return this.orderForm.get('customerAddress'); }
  get warehouse() { return this.orderForm.get('warehouse'); }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const id: number = +params.get('id');

      if (id) {
        this.service.findById(id).subscribe(order => {
          this.order = order;
          this.isShow = true;
          this.orderDetail = this.order.orderDetail;
          this.formSetting();
          this.showColumns = ['position', 'sku', 'name', 'quantity'];
          this.orderForm.get('customer').setValue(this.order.customer);
          this.orderForm.get('customerAddress').setValue(this.order.customer.address);
          this.orderForm.get('warehouse').setValue(this.order.warehouse);

          this.paginatorInit();
          this.service.getCustomersList().subscribe(customers => {
            this.customers = customers;
            this.service.getWarehousesList().subscribe(warehouses => {
              this.warehouses = warehouses;
            });
          });
        });
      } else {
        this.isShow = false;
        this.showColumns = ['position', 'sku', 'name', 'quantity', 'actions'];
        this.formSetting();
        this.paginatorInit();
        this.service.getCustomersList().subscribe(customers => {
          this.customers = customers;
          this.service.getWarehousesList().subscribe(warehouses => {
            this.warehouses = warehouses;
          });
        });
      }
    });
  }

  formSetting(): void {
    this.orderForm = new FormGroup({
      customer: new FormControl({value: '', disabled: this.isShow}, [Validators.required, Validators.minLength(5)]),
      customerAddress: new FormControl(''),
      warehouse: new FormControl({value: '', disabled: this.isShow}, [Validators.required, Validators.minLength(10), Validators.maxLength(250)])
    });
  }

  setAcdress(): void {
    this.orderForm.get('customerAddress').setValue(this.orderForm.get('customer').value.address);
  }

  getProducts(): void {
    const warehouseToSearch = this.orderForm.get('warehouse').value;
    this.service.getProductsByWarehouse(warehouseToSearch.id).subscribe(products => {
      this.products = products;
      this.productPaginatorInit();
    });
  }

  private paginatorInit(): void {
    this.dataSource = new MatTableDataSource<OrderDetail>(this.orderDetail);
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Registros por página';
  }

  private productPaginatorInit(): void {
    this.dataSourceProduct = new MatTableDataSource<Product>(this.products);
    this.dataSourceProduct.paginator = this.paginatorProducts;
    this.paginatorProducts._intl.itemsPerPageLabel = 'Registros por página';
  }

  addProduct(product: Product): void {
    this.products = this.products.filter(p => p.id !== product.id);
    this.productPaginatorInit();

    this.detail = new OrderDetail();
    this.detail.product = product;
    this.detail.quantity = 0;

    this.orderDetail = this.orderDetail.concat(this.detail);

    this.paginatorInit();
  }

  quantityManagement(detail: OrderDetail): void {
    this.newQuantity = 0;
    const modalRef = this.dialog.open(OrdersModalComponent, {
      width: '750px',
      data: {
        detail: detail,
        newQuantity: this.newQuantity,
        productName: detail.product.sku + ' - ' + detail.product.name,
        warehouseId: this.orderForm.get('warehouse').value.id
      }
    });

    this.newQuantity = detail.quantity;

    modalRef.afterClosed().subscribe(newDetail => {
      if (newDetail) {
        if (newDetail.quantity >= 0) {
          this.orderDetail.forEach(od => {
            if (od.product.id === newDetail.product.id) {
              od.quantity = newDetail.quantity;
            }
          });   
        } else {
          newDetail.quantity = this.newQuantity;
          Swal.fire('Error:', `No es posible disminuir la cantidad solicitada, por favor ingrese un numero valido`, 'error');
        }
      }
    });
  }

  orderValid(): boolean {
    var quantities: number[] = [];
    
    if (this.orderForm.valid && this.orderDetail.length > 0) {
      this.orderDetail.map(oD => {
        quantities = quantities.concat(oD.quantity);
      });

      const isBelowThreshold = (currentValue) => currentValue > 0;

      return !quantities.every(isBelowThreshold);
    }

    return true;
  }

  confirm(): void {
    this.order.customer = this.orderForm.get('customer').value;
    this.order.warehouse = this.orderForm.get('warehouse').value;
    
    this.order.orderDetail = this.orderDetail;
      
    this.service.store(this.order).subscribe(order => {
      Swal.fire('Nuevo: ',`${order.id} para el cliente (${order.customer.dni}) - ${order.customer.name} ${order.customer.surname} creado con éxito`, 'success');
      this.router.navigate(['/orders']);
    });
  }

  removeProduct(detail: OrderDetail): void {
    this.orderDetail = this.orderDetail.filter(od => od.id !== detail.id);
    this.paginatorInit();
    this.products = this.products.concat(detail.product);
    this.productPaginatorInit();
  }

  cancel(): void {
    this.router.navigate(['/orders']);
  }
}
