import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OrderDetail } from 'src/app/models/order-detail';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OrderService } from 'src/app/services/order.service';
import { Warehouse } from 'src/app/models/warehouse';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-orders-modal',
  templateUrl: './orders-modal.component.html',
  styleUrls: []
})
export class OrdersModalComponent implements OnInit {

  newQuantity: number = 0;
  detailForm: FormGroup;
  detail: OrderDetail = new OrderDetail();
  warehouseId: number = 0;

  productName: string = '';
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public modalRef: MatDialogRef<OrdersModalComponent>,
    private service: OrderService
  ) {
    this.detailForm = new FormGroup({
      quantity: new FormControl('',
        [
          Validators.required,
          Validators.pattern("^[1-9]+[0-9]*$")
        ])
    });
  }

  get quantity() { return this.detailForm.get('quantity'); }

  ngOnInit(): void {
    this.newQuantity = this.data.newQuantity as number;
    this.detail = this.data.detail as OrderDetail;
    this.productName = this.data.productName as string;
    this.warehouseId = this.data.warehouseId as number;
    this.detailForm.get('quantity').setValue(this.newQuantity);
  }

  add(): void {
    this.service.getStockByWarehouseAndProduct(this.warehouseId, this.detail.product.id).subscribe(qty => {
      const quantity = (+this.detailForm.value.quantity);
      if (qty > quantity) {
        this.detail.quantity = (this.detail.quantity + (+this.detailForm.value.quantity));
        this.detailForm.reset();
      } else {
        Swal.fire('Error: ', 'La cantidad solicitada no puede ser cubierta por el almacén', 'error');
      } 
    });
    
  }

  decrease(): void {
    const quantity = +this.detailForm.value.quantity;
    this.detail.quantity = (this.detail.quantity - quantity);
    this.detailForm.reset();
  }

}
