import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OrderService } from 'src/app/services/order.service';
import { Order } from 'src/app/models/order';
import { OrderStatus } from 'src/app/models/order-status';

@Component({
  selector: 'app-status-update-modal',
  templateUrl: './status-update-modal.component.html',
  styleUrls: []
})
export class StatusUpdateModalComponent implements OnInit {

  order: Order = new Order();
  statusForm: FormGroup;
  statusList: OrderStatus[] = [];
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public modalRef: MatDialogRef<StatusUpdateModalComponent>,
    private service: OrderService
  ) {
    this.statusForm = new FormGroup({
      statusField: new FormControl('',
        [
          Validators.required
        ])
    });
  }

  get statusField() { return this.statusForm.get('statusField'); }

  ngOnInit(): void {
    this.order = this.data.order as Order;
    
    this.statusForm.get('statusField').setValue(this.order.status);
    this.service.getStatusList().subscribe(statusList => {
      this.statusList = statusList;
    });
  }

  confirm(): void {
    this.order.status = this.statusForm.get('statusField').value;
  }

  cancel(): void {
    this.modalRef.close();
  }

}
