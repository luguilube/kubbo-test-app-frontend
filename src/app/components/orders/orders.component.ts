import { Component, OnInit, ViewChild } from '@angular/core';
import { Order } from 'src/app/models/order';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { OrderService } from 'src/app/services/order.service';
import { MatDialog } from '@angular/material/dialog';
import { StatusUpdateModalComponent } from './status-update-modal/status-update-modal.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: []
})
export class OrdersComponent implements OnInit {

  orders: Order[] = [];
  product: Order = new Order();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  showColumns: string[] = ['position', 'id', 'customer', 'shippingDate', 'deliveryDate', 'status', 'actions'];
  pageSizeOptions: number[] = [3, 5, 10, 20, 30, 50];
  totalFields: number = 0;
  totalByPage: number = 3;
  currentPage: number = 0;

  constructor(
    private service: OrderService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.rangesCalculate();
  }

  paginate(event: PageEvent): void {
    this.currentPage = event.pageIndex;
    this.totalByPage = event.pageSize;
    this.rangesCalculate();
  }

  private rangesCalculate() {
    this.service.findAllPaginated(this.currentPage.toString(), this.totalByPage.toString())
    .subscribe(page => {
      this.orders = page.content as Order[];
      this.totalFields = page.totalElements as number;
      this.paginator._intl.itemsPerPageLabel = 'Registros por página';
    });
  }
  
  edit(order: Order): void {
    const modalRef = this.dialog.open(StatusUpdateModalComponent, {
      width: '750px',
      data: {order: order} 
    });

    modalRef.afterClosed().subscribe(orderUpdated => {
      if (orderUpdated) {
        this.service.orderStatusUpdate(orderUpdated.id, orderUpdated.status.id).subscribe(o => {
          this.showUpdatedItem(o as Order);
          this.rangesCalculate();
          Swal.fire('Modificado:', `Se ha actualizado el estatus para la orden: ${order.id} con exito`, 'success');
        }); 
      }
    });
  }



  delete(order: Order): void { 
    Swal.fire({
      title: 'Advertencia:',
      text: `¿Seguro que desea eliminar a ${order.id}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar'
    }).then((result) => {
      if (result.value) {
        this.service.delete(order.id).subscribe(() => {
          this.orders = this.orders.filter(o => o.id !== order.id);
          Swal.fire('Eliminado: ',`${order.id}} eliminada con éxito`, "success");
        });
      }
    });
  }

  showUpdatedItem(newItem: Order){
    let updateItem = this.orders.find(this.findIndexToUpdate, newItem.id);

    let index = this.orders.indexOf(updateItem);

    this.orders[index] = newItem;

  }

  findIndexToUpdate(newItem) { 
        return newItem.id === this;
  }

}
