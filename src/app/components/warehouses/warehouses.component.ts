import { Component, OnInit, ViewChild } from '@angular/core';
import { Warehouse } from 'src/app/models/warehouse';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { WarehouseService } from 'src/app/services/warehouse.service';
import { MatDialog } from '@angular/material/dialog';
import { WarehousesModalComponent } from './warehouses-modal/warehouses-modal.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-warehouses',
  templateUrl: './warehouses.component.html',
  styleUrls: []
})
export class WarehousesComponent implements OnInit {

  warehouses: Warehouse[] = [];
  warehouse: Warehouse = new Warehouse();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  showColumns: string[] = ['position', 'name', 'warehouseIdentifier', 'actions'];
  pageSizeOptions: number[] = [3, 5, 10, 20, 30, 50];
  totalFields: number = 0;
  totalByPage: number = 3;
  currentPage: number = 0;

  constructor(
    private service: WarehouseService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.rangesCalculate();
  }

  paginate(event: PageEvent): void {
    this.currentPage = event.pageIndex;
    this.totalByPage = event.pageSize;
    this.rangesCalculate();
  }

  private rangesCalculate() {
    this.service.findAllPaginated(this.currentPage.toString(), this.totalByPage.toString())
    .subscribe(page => {
      this.warehouses = page.content as Warehouse[];
      this.totalFields = page.totalElements as number;
      this.paginator._intl.itemsPerPageLabel = 'Registros por página';
    });
  }

  create(): void {
    this.warehouse = new Warehouse();
    const modalRef = this.dialog.open(WarehousesModalComponent, {
      width: '750px',
      data: {warehouse: this.warehouse, title: 'Crear '} 
    });

    modalRef.afterClosed().subscribe(warehouse => {
      if (warehouse) {
        this.service.store(warehouse).subscribe(p => {
          this.warehouses = this.warehouses.concat(p);
          Swal.fire('Creado:', `Se ha registrado: ${warehouse.warehouseIdentifier} - ${warehouse.name} con exito`, 'success');
        }); 
      }
    });
  }

  show(warehouse: Warehouse): void {
    const modalRef = this.dialog.open(WarehousesModalComponent, {
      width: '750px',
      data: {warehouse: warehouse, title: 'Ver ', isShow: true} 
    });
  }
  
  edit(warehouse: Warehouse): void {
    const modalRef = this.dialog.open(WarehousesModalComponent, {
      width: '750px',
      data: {warehouse: warehouse, title: 'Editar '} 
    });

    modalRef.afterClosed().subscribe(warehouse => {
      if (warehouse) {
        this.service.update(warehouse).subscribe(p => {
          this.showUpdatedItem(p as Warehouse);
          Swal.fire('Modificado:', `Se ha actualizado: ${warehouse.warehouseIdentifier} - ${warehouse.name} con exito`, 'success');
        }); 
      }
    });
  }

  delete(warehouse: Warehouse): void { 
    Swal.fire({
      title: 'Advertencia:',
      text: `¿Seguro que desea eliminar a ${warehouse.warehouseIdentifier} - ${Warehouse.name}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar'
    }).then((result) => {
      if (result.value) {
        this.service.delete(warehouse.id).subscribe(() => {
          this.warehouses = this.warehouses.filter(p => p.id !== warehouse.id);
          Swal.fire('Eliminado: ',`${warehouse.warehouseIdentifier} - ${warehouse.name} eliminado con éxito`, "success");
        });
      }
    });
  }

  showUpdatedItem(newItem: Warehouse){
    let updateItem = this.warehouses.find(this.findIndexToUpdate, newItem.id);

    let index = this.warehouses.indexOf(updateItem);

    this.warehouses[index] = newItem;
  }

  findIndexToUpdate(newItem) { 
        return newItem.id === this;
  }

}
