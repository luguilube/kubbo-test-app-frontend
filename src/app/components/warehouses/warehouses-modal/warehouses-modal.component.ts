import { Component, OnInit, Inject } from '@angular/core';
import { Warehouse } from 'src/app/models/warehouse';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-warehouses-modal',
  templateUrl: './warehouses-modal.component.html',
  styleUrls: []
})
export class WarehousesModalComponent implements OnInit {

  warehouse: Warehouse = new Warehouse();
  title: String = '';
  isShow: boolean = false;

  public warehouseForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public modalRef: MatDialogRef<WarehousesModalComponent>
  ) {
    this.warehouseForm = new FormGroup({
      name: new FormControl('', [Validators.minLength(5)]),
      warehouseIdentifier: new FormControl('', [Validators.required, Validators.minLength(3)]),
      address: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(250)])
    });
  }

  get name() { return this.warehouseForm.get('name'); }
  get warehouseIdentifier() { return this.warehouseForm.get('warehouseIdentifier'); }
  get address() { return this.warehouseForm.get('address'); }

  ngOnInit(): void {
    this.warehouse = this.data.warehouse as Warehouse;
    this.title = this.data.title as string;
    this.isShow = this.data.isShow ? this.data.isShow as boolean : false;

    this.warehouseForm.get('name').setValue(this.warehouse.name);
    this.warehouseForm.get('warehouseIdentifier').setValue(this.warehouse.warehouseIdentifier);
    this.warehouseForm.get('address').setValue(this.warehouse.address);
  }

  cancel(): void {
    this.modalRef.close();
  }

  confirm(): void {
    this.warehouse.name = this.warehouseForm.value.name;
    this.warehouse.warehouseIdentifier = this.warehouseForm.value.warehouseIdentifier;
    this.warehouse.address = this.warehouseForm.value.address;
    this.warehouseForm.reset();
  }

}
