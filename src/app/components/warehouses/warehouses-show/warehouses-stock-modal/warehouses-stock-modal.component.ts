import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from 'src/app/models/product';
import { ProductWarehouse } from 'src/app/models/product-warehouse';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-warehouses-stock-modal',
  templateUrl: './warehouses-stock-modal.component.html',
  styleUrls: []
})
export class WarehousesStockModalComponent implements OnInit {

  newQuantity: number = 0;
  stockForm: FormGroup;
  product: ProductWarehouse = new ProductWarehouse();

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public modalRef: MatDialogRef<WarehousesStockModalComponent>
  ) {
    this.stockForm = new FormGroup({
      quantity: new FormControl('',
        [
          Validators.required,
          Validators.pattern("^[1-9]+[0-9]*$")
        ])
    });
  }

  get quantity() { return this.stockForm.get('quantity'); }

  ngOnInit(): void {
    this.newQuantity = this.data.newQuantity as number;
    this.product = this.data.product as ProductWarehouse;
    this.stockForm.get('quantity').setValue(this.newQuantity);
  }

  add(): void {
    this.product.quantity = (this.product.quantity + (+this.stockForm.value.quantity));
    this.stockForm.reset();
  }

  decrease(): void {
    const quantity = +this.stockForm.value.quantity;
    this.product.quantity = (this.product.quantity - quantity);
    this.stockForm.reset();
  }

}
