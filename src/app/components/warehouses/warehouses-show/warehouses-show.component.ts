import { Component, OnInit, ViewChild } from '@angular/core';
import { Warehouse } from 'src/app/models/warehouse';
import { ActivatedRoute } from '@angular/router';
import { WarehouseService } from 'src/app/services/warehouse.service';
import { ProductWarehouse } from 'src/app/models/product-warehouse';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { WarehousesStockModalComponent } from './warehouses-stock-modal/warehouses-stock-modal.component';
import { Product } from 'src/app/models/product';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-warehouses-show',
  templateUrl: './warehouses-show.component.html',
  styleUrls: []
})
export class WarehousesShowComponent implements OnInit {

  warehouse: Warehouse = new Warehouse();
  products: ProductWarehouse[] = [];
  productsNotAssigned: Product[] = [];
  newQuantity: number = 0;
  productWarehouse: ProductWarehouse = new ProductWarehouse();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  showColumns: string[] = ['position', 'sku', 'name', 'quantity', 'actions'];
  pageSizeOptions: number[] = [3, 5, 10, 20, 30, 50];
  dataSource: MatTableDataSource<ProductWarehouse>;

  @ViewChild(MatPaginator, { static: true }) paginatorProducts: MatPaginator;
  showProductColumns: string[] = ['position', 'sku', 'name', 'actions'];
  pageSizeProductsOptions: number[] = [3, 5, 10, 20, 30, 50];
  dataSourceProduct: MatTableDataSource<Product>;

  tabIndex = 0;

  constructor(
    private service: WarehouseService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const id: number = +params.get('id');

      if (id) {
        this.service.get(id).subscribe(warehouse => {
          this.warehouse = warehouse;
          this.products = this.warehouse.products;
          this.paginatorInit();
          this.getAvailableProductsNotAssigned(id);
        });
      }
    });
  }

  private paginatorInit(): void {
    this.dataSource = new MatTableDataSource<ProductWarehouse>(this.products);
    this.dataSource.paginator = this.paginator;
    this.paginator._intl.itemsPerPageLabel = 'Registros por página';
  }

  getAvailableProductsNotAssigned(id: number): void {
    this.service.getAvailableProductsNotAssigned(id).subscribe(productsNotassigned => {
      this.productsNotAssigned = productsNotassigned;
      this.productPaginatorInit();
    });
  }

  private productPaginatorInit(): void {
    this.dataSourceProduct = new MatTableDataSource<Product>(this.productsNotAssigned);
    this.dataSourceProduct.paginator = this.paginatorProducts;
    this.paginatorProducts._intl.itemsPerPageLabel = 'Registros por página';
  }

  manage(product: ProductWarehouse): void {
    this.newQuantity = 0;
    const modalRef = this.dialog.open(WarehousesStockModalComponent, {
      width: '750px',
      data: { product: product, newQuantity: this.newQuantity}
    });
    
    this.newQuantity = product.quantity;

    modalRef.afterClosed().subscribe(productW => {
      if (productW) {
        if (productW.quantity >= 0) {
          this.showUpdatedItem(productW as ProductWarehouse);
          this.warehouse.products = this.products;
          this.service.stockManagement(this.warehouse).subscribe(p => {
            Swal.fire('Modificado:', `Se ha actualizado con exitosamente el stock del producto`, 'success');
          });
        } else {
          productW.quantity = this.newQuantity;
          Swal.fire('Error:', `No es posible disminuir la cantidad solicitada, por favor ingrese un numero valido`, 'error');
        }
      }
    });
  }

  showUpdatedItem(newItem: ProductWarehouse){
    let updateItem = this.products.find(this.findIndexToUpdate, newItem.id);

    let index = this.products.indexOf(updateItem);

    this.products[index] = newItem;
  }

  findIndexToUpdate(newItem) { 
        return newItem.id === this;
  }

  delete(id: number, productW: ProductWarehouse): void {
    if (productW.quantity == 0) {
      Swal.fire({
        title: 'Advertencia:',
        text: `¿Seguro que desea eliminar a ${productW.product.sku} - ${productW.product.name} de este almacén?`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar'
      }).then((result) => {
        if (result.value) {
          this.service.productDeleteFromStock(id, productW.id).subscribe(products => {
            this.products = this.products.filter(a => a.id !== productW.id);
            this.warehouse.products = this.products; 
            this.getAvailableProductsNotAssigned(this.warehouse.id);
            this.paginatorInit();
            Swal.fire('Eliminado: ',`${productW.product.sku} - ${productW.product.name} eliminado con éxito`, "success");
          });
        }
      });
    } else {
      Swal.fire('Error: ',`No es posible eliminar ${productW.product.sku} - ${productW.product.name} porque aun existen unidades disponibles en almacén`, "error");
    }
  }

  addToWarehouse(product: Product): void {
    this.productWarehouse.product = product;
    this.productWarehouse.warehouse = this.warehouse;
    this.productWarehouse.quantity = 0;
    
    this.service.addProductToWarehouse(this.productWarehouse).subscribe(w => {
      this.warehouse = w;
      this.products = this.warehouse.products;
      this.products = this.products.filter(p => p.deleted === w.deleted);
      this.paginatorInit();
      this.getAvailableProductsNotAssigned(this.warehouse.id);
      Swal.fire('Registrado:', `Se ha agregado exitosamente el producto ${product.sku} - ${product.name} al almacén del producto`, 'success');
    });
  }
}
