import { Component, OnInit, Inject } from '@angular/core';
import { Product } from 'src/app/models/product';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: []
})
export class ModalComponent implements OnInit {

  product: Product = new Product();
  title: String = '';
  isShow: boolean = false;

  public productForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public modalRef: MatDialogRef<ModalComponent>
  ) {
    this.productForm = new FormGroup({
      sku: new FormControl('', [Validators.required, Validators.minLength(1)]),
      name: new FormControl('', [Validators.required, Validators.minLength(5)]),
      description: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(250)]),
      enabled: new FormControl()
    });
  }

  get sku() { return this.productForm.get('sku'); }
  get name() { return this.productForm.get('name'); }
  get description() { return this.productForm.get('description'); }
  get enabled() { return this.productForm.get('enabled'); }

  ngOnInit(): void {
    this.product = this.data.product as Product;
    this.title = this.data.title as string;
    this.isShow = this.data.isShow ? this.data.isShow as boolean : false;

    this.productForm.get('sku').setValue(this.product.sku);
    this.productForm.get('name').setValue(this.product.name);
    this.productForm.get('description').setValue(this.product.description);
    this.productForm.get('enabled').setValue(this.product.enabled);
    this.isShow ? this.productForm.get('enabled').disable({onlySelf: true}) : this.productForm.get('enabled').enable;
  }

  cancel(): void {
    this.modalRef.close();
  }

  confirm(): void {
    this.product.sku = this.productForm.value.sku;
    this.product.name = this.productForm.value.name;
    this.product.description = this.productForm.value.description;
    const checked = this.productForm.get('enabled');
    this.product.enabled = checked.value ? true : false;
    this.productForm.reset();
  }
}
