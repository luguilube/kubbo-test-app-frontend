import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from 'src/app/models/product';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { ProductService } from 'src/app/services/product.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from './modal/modal.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: []
})
export class ProductsComponent implements OnInit {

  products: Product[] = [];
  product: Product = new Product();

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  showColumns: string[] = ['position', 'sku', 'name', 'enabled', 'actions'];
  pageSizeOptions: number[] = [3, 5, 10, 20, 30, 50];
  totalFields: number = 0;
  totalByPage: number = 3;
  currentPage: number = 0;

  constructor(
    private service: ProductService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.rangesCalculate();
  }

  paginate(event: PageEvent): void {
    this.currentPage = event.pageIndex;
    this.totalByPage = event.pageSize;
    this.rangesCalculate();
  }

  private rangesCalculate() {
    this.service.findAllPaginated(this.currentPage.toString(), this.totalByPage.toString())
    .subscribe(page => {
      this.products = page.content as Product[];
      this.totalFields = page.totalElements as number;
      this.paginator._intl.itemsPerPageLabel = 'Registros por página';
    });
  }

  create(): void {
    this.product = new Product();
    const modalRef = this.dialog.open(ModalComponent, {
      width: '750px',
      data: {product: this.product, title: 'Crear '} 
    });

    modalRef.afterClosed().subscribe(product => {
      if (product) {
        this.service.store(product).subscribe(p => {
          this.products = this.products.concat(p);
          Swal.fire('Creado:', `Se ha registrado: ${product.sku} - ${product.name} con exito`, 'success');
        }); 
      }
    });
  }

  show(product: Product): void {
    const modalRef = this.dialog.open(ModalComponent, {
      width: '750px',
      data: {product: product, title: 'Ver ', isShow: true} 
    });
  }
  
  edit(product: Product): void {
    const modalRef = this.dialog.open(ModalComponent, {
      width: '750px',
      data: {product: product, title: 'Editar '} 
    });

    modalRef.afterClosed().subscribe(product => {
      if (product) {
        this.service.update(product).subscribe(p => {
          this.showUpdatedItem(p as Product);
          Swal.fire('Modificado:', `Se ha actualizado el producto sku: ${product.sku} - ${product.name} con exito`, 'success');
        }); 
      }
    });
  }

  delete(product: Product): void { 
    Swal.fire({
      title: 'Advertencia:',
      text: `¿Seguro que desea eliminar a ${product.sku} - ${product.name}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar'
    }).then((result) => {
      if (result.value) {
        this.service.delete(product.id).subscribe(() => {
          this.products = this.products.filter(p => p.id !== product.id);
          Swal.fire('Eliminado: ',`${product.sku} - ${product.name} eliminado con éxito`, "success");
        });
      }
    });
  }

  showUpdatedItem(newItem: Product){
    let updateItem = this.products.find(this.findIndexToUpdate, newItem.id);

    let index = this.products.indexOf(updateItem);


    this.products[index] = newItem;

  }

  findIndexToUpdate(newItem) { 
        return newItem.id === this;
  }

}
