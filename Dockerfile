## Stage 1: Build
FROM node:10.22-alpine AS build

WORKDIR /app

COPY package.json ./

RUN npm install

COPY . .

RUN npm run build --prod


## Stage 2: Run
FROM nginx:1.19-alpine AS prod-stage

COPY --from=build /app/dist/kubbo-test-app-frontend /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]