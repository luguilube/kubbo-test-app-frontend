INSERT INTO kubbo_test_app.order_status (created_at,deleted,description,name) VALUES 
('2020-06-21 14:57:37.000',0,'Estatus inicial de una orden','Creado')
,('2020-06-21 14:57:37.000',0,'Estatus para indicar que el almacén se encuentra preparando el envío','Preparando')
,('2020-06-21 14:57:37.000',0,'Indica que el pedido ha sido enviado al destino','Enviado')
,('2020-06-21 14:57:37.000',0,'Estatus final de la orden','Entregado')
;