
cd ../../kubbo-config-server
mvn spring-boot:run &

cd ../../kubbo-eureka-server
mvn spring-boot:run &

cd ../kubbo-customer-microservice
mvn spring-boot:run  &

cd ../kubbo-users-microservice
mvn spring-boot:run  &

cd ../kubbo-product-microservice
mvn spring-boot:run &

cd ../kubbo-warehouse-microservice
mvn spring-boot:run  &

cd ../kubbo-order-microservice
mvn spring-boot:run &

cd ../kubbo-cloud-gateway
mvn spring-boot:run  &

cd ../kubbo-test-app-frontend
ng serve -o  &
