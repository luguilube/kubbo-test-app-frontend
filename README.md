## Kubbo Test App

Kubbo Test App es una app basada en una idea inicial de la empresa Kubbo, específicamente como prueba técnica para postulación a una vacante. Esta app fue diseñada por un conjunto de 6 microservicios para el backend  (servidor, customers, products, warehouses, orders, cloud gateway) para gestionar específicamente lo referente a cada funcionalidad  requerida por la App, pero segmentada en módulos; y para el frontend un proyecto únicamente. 

## Herramientas Utilizadas

Este proyecto fue desarrollado con el framework Spring Boot en su versión 2.3.1 para la creación de los microservicios backend, también haciendo uso de herramientas tales como Maven, Spring Data JPA, Spring Cloud Netflix Eureka Server y Client, Spring Cloud Gateway el cual implementa Loadbalancer, Open Feign, Postman, se utilizó como motor de base de datos  MariaDB(fork de Mysql).

Respecto al frontend se utilizó Angular en su versión 9 y también haciendo uso de herramientas como nodejs, npm, HTML, CSS, Bootstrap 4, Angular Material, Sweet Alert 2, Fontawesome.

Con relación a herramientas para codificación, manejo de base de datos, y prueba de endpoints de los microservicios se usaron [Spring Boot Tool Suite](https://spring.io/tools)  para Backend, [Visual Studio Code](https://code.visualstudio.com/)  para Frontend, [Postman](https://www.postman.com/) para testeo de endpoints, como manejador de base de datos [DBeaver Community](https://dbeaver.io/) y [MySQL Workbench](https://dbeaver.io/). 

## Alojado en un Repositorio

- El proyecto se encuentra alojado en un proyecto llamado KuboTestApp, el cual esta compuesto por 7 repositorios GIT, 1 por cada microservicio y front de la app en [Bitbucket](https://bitbucket.org/luguilube/workspace/projects/KUB).

## Requisitos Técnicos

Se deben tener instalados las siguientes tecnologías.

- GIT.
- Java con un JDK 11 o superior.
- MariaDB (Fork de MySQL), en los microservicios está configurada la conexión a la base de datos con una password de ejemplo, debe modificarse este valor y adecuarse a la contraseña que se tenga configurada en su motor de MariaDB.
- Crear una base de datos vacia llamada: kubbo_test_app.
- Maven instalado de forma global.
- Nodejs instalado de forma global.
- NPM.
- Angular.
- Docker.
- Docker-Compose.

## Nota:

Al momento de desarrollar esta prueba, ya se disponen de las tecnologías anteriormente mencionadas y tal como se mencionó anteriormente, fue desarrollado en Windows 10, por tal motivo los siguientes pasos mencionados pueden variar en la actualidad, se brinda una explicación base sencilla para Windows y Ubuntu con enlaces de referencia.

## Instalación y Ejecución

Por simplicidad de instalación debido a que fue desarrollado para una prueba, los archivos de configuracion como el application.properties y application.yml se encuentran en el repositorio, también junto a esta entrega en el proyecto front de la app “kubbo-test-app-frontend”, se encuentra una carpeta con entregables, tales como un Diagrama Entidad - Relación y Proyecto Workbench, Colecciones Postman, un archivo SQL con los datos por defecto para una entidad maestra que gestiona los estados de la orden y un archivo init_script.sh el cual permitia la ejecucion del proyecto previamente antes de la implementacion de docker; este archivo solo esta de muestra mas no permite en la actualidad ejecutar los proyectos.

Si no se disponen de las tecnologías anteriormente mencionadas en la sección de requisitos técnicos. los pasos para instalarlos se listan a continuación:

- Instalación de Git: 
    - [Windows](https://git-scm.com/). 
    - [Ubuntu](https://www.digitalocean.com/community/tutorials/como-instalar-git-en-ubuntu-18-04-es).
  

-  Instalación de JDK 11:
    - [Windows](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html). 
    - [Ubuntu](https://www.serverlab.ca/scripting-programming/how-to-install-java-11-on-ubuntu-18-04/).
  

- Crear una base de datos vacía llamada kubbo_test_app: 
    - [Enlace](https://dev.mysql.com/doc/refman/8.0/en/creating-database.html)

    Otra forma de poder hacer esto es si se dispone de un manejador de base de datos tal como Dbeaver, MySQL Workbench o cualquier otro y hacerlo mediante su interfaz gráfica.

    [Mysql Workbench](https://www.ecodeup.com/como-crear-una-base-de-datos-utilizando-mysql-y-mysql-workbench/).
    [Dbeaver](https://github.com/dbeaver/dbeaver/wiki/Create-Connection).
  

- Instalar Maven de forma global:
    - [Windows](https://mkyong.com/maven/how-to-install-maven-in-windows/). 
    - [Ubuntu](https://linuxize.com/post/how-to-install-apache-maven-on-ubuntu-18-04/).
  

- Instalar Nodejs y NPM:
    - [Windows](https://nodejs.org/es/). 
    - [Ubuntu](https://hostadvice.com/how-to/how-to-install-node-js-on-ubuntu-18-04/).
  

- Instalar Angular:
    - [Angular](https://angular.io/guide/setup-local#install-the-angular-cli).

    Ya teniendo instalado npm, independientemente del SO que se esté utilizando, se debe ejecutar el siguiente comando en una terminal:

    npm install -g @angular/cli

    Si es ubuntu y da error de permisos, recordar usar primeramente: 
    sudo npm install -g @angular/cli  

- Clonar todos los repositorios del proyecto [Bitbucket](https://bitbucket.org/luguilube/workspace/projects/KUB):

    Idealmente todos en una misma carpeta, para esto, desde una terminal; se debe posicionar dentro del directorio del ordenador donde se desea alojar el proyecto, dirigirse al enlace y dentro cada repositorio dentro del proyecto, hacer click en el botón que se encuentra en la esquina superior derecha de la página llamado "Clone" o "Clonar", y copiar el enlace HTTPS.
  

### Configuración Docker

- [Windows](https://docs.docker.com/docker-for-windows/install/)
- [Linux](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-es)

Ya contando con Docker y/o Docker Compose, los pasos a seguir son los siguientes:

- Ingresar dentro de cada directorio de los microservicios exceptuando “kubbo-test-app-frontend” y ejecutar los siguientes comandos mediante una terminal.
    - mvn clean package -DskipTests (Este comando permite la descarga de las dependencias y la generacion del archivo jar compilado).
    - docker build -t nombre-proyecto:v1 . (Este comando crea una imagen docker local; un ejemplo de comando sería: kubbo-config-server:v1 . )

- Nota:
Si hay dudas respecto al nombre para colocar, dentro del proyecto kubbo-docker-compose y dentro del archivo docker-compose.yml, basta con fijarse en cada una de las etiquetas “image” y el nombre justo al lado.

- Si no se tiene instalado una version de MariDB / MySQL, es posible descargar una imagen desde [Docker Hub](https://hub.docker.com/_/mysql), si se llega a tener alguna instalación de MariDB / MySQL, debe detenerse el servicio del mismo para poder trabajar con la imagen docker.

- El siguiente paso es ingresar a la carpeta del proyecto kubbo-test-app-frontend, y se deben ejecutar los siguientes comandos:
    - ng build --prod (Este comando descarga y compila las dependencias del proyecto en formato ligero).
    - docker build -t kubbo-test-app-frontend:v1 . (Este comando crea una imagen docker local)

- Nota:
Si ocurre algun error al momento de ejecutar el comando “ng build --prod” debe ejecutarse entonces el comando “npm install”.

finalmente luego de haber generado con los pasos anteriores las imágenes docker para cada uno de los proyectos, abriremos el archivo “docker-compose.yml” que se encuentra dentro del proyecto “kubbo-docker-compose” y procederemos a ejecutar cada uno de los contenedores en el mismo orden como están especificados alli; es muy importante dependiendo de si no se cuenta con una computadora con buenas características, esperar un tiempo prudencial aproximadamente de al menos 10 segundos al ejecutar cada contenedor puesto que no solo basta con ejecutar el contenedor sino que también dentro del contenedor los aplicativos se ejecutan y empiezan a configurarse y eso puede demorar nuevamente mucho o poco dependiendo del equipo con el que se ejecute. 

Se procede a ejecutar cada uno de los contenedores con el siguiente comando:

docker-compose up -d nombre-contenedor. Ejm: docker-compose up -d kubbo-config-server

Tras haber realizado los pasos anteriores ya se podra acceder a la url localhost:8000 y usar observar y probar la app.



## Sugerencias y Correcciones

Ante cualquier error, sugerencias, o comentario, por favor indicarme vía correo a elluisluna@gmail.com, estare muy agradecido.